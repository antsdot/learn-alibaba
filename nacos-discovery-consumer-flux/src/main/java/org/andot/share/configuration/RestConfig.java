package org.andot.share.configuration;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author andot
 */
@Configuration
public class RestConfig {
    @Bean
    @LoadBalanced
    public WebClient.Builder webClient () {
        return WebClient.builder();
    }
}
