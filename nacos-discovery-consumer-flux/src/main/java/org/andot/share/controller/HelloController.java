package org.andot.share.controller;

import com.alibaba.cloud.nacos.ribbon.NacosServer;
import org.andot.share.service.HaloFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.RibbonLoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author andot
 */
@RestController
@RequestMapping("/hello")
public class HelloController {
    @Autowired
    private WebClient.Builder webClient;
    @Resource
    private HaloFeignService haloFeignService;

    @GetMapping("")
    public Mono<String> getName () {
        return webClient.build()
                .get()
                .uri("http://provider/hello/"+UUID.randomUUID().toString())
                .retrieve().bodyToMono(String.class);
    }

    @GetMapping("/feign")
    public ResponseEntity<String> getFeignName () {
        return ResponseEntity.ok(haloFeignService.getName(UUID.randomUUID().toString()));
    }

}
