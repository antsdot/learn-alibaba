package org.andot.share;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author andot
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class NacosDiscoveryConsumerFluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosDiscoveryConsumerFluxApplication.class, args);
    }

}
