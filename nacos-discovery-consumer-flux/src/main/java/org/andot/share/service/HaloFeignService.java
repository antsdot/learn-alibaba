package org.andot.share.service;

import org.andot.share.fallback.HaloFallback;
import org.andot.share.fallback.HaloFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


/**
 * @author andot
 */
@Service("haloFeignService")
@FeignClient(name = "provider", path = "/hello",
        fallbackFactory = HaloFallbackFactory.class, fallback = HaloFallback.class)
public interface HaloFeignService {

    @GetMapping("/{id}")
    String getName(@PathVariable("id") String id);
}
