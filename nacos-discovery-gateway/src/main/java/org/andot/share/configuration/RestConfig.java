package org.andot.share.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author andot
 */
@Configuration
public class RestConfig {
    @Bean
    public RestTemplate restTemplate () {
        return new RestTemplate();
    }
}
