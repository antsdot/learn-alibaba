package org.andot.share.fallback;

import feign.hystrix.FallbackFactory;
import org.andot.share.service.HaloFeignService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author andot
 */
@Component
public class HaloFallbackFactory implements FallbackFactory<ResponseEntity> {
    @Override
    public ResponseEntity create(Throwable throwable) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body("阿偶，系统开小差了！");
    }
}
