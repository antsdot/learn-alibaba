package org.andot.share.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 白
 * @author andot
 */
@RestController
@RequestMapping("/security")
public class SecurityController {
    @GetMapping("/white")
    public String getWhite () {
        return "white";
    }
    @GetMapping("/black")
    public String getBlack () {
        return "black";
    }
}
