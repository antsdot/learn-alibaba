package org.andot.share.controller;

import com.alibaba.cloud.nacos.ribbon.NacosServer;
import org.andot.share.service.HaloFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.netflix.ribbon.RibbonLoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @author andot
 */
@RestController
@RequestMapping("/hello")
public class HelloController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private LoadBalancerClient loadBalancerClient;
    @Resource
    private HaloFeignService haloFeignService;

    @GetMapping("")
    public ResponseEntity<String> getName () {
        RibbonLoadBalancerClient.RibbonServer ribbonServer = (RibbonLoadBalancerClient.RibbonServer)loadBalancerClient.choose("provider");
        NacosServer nacosServer = (NacosServer) ribbonServer.getServer();
        System.err.println(nacosServer.getMetadata());
        return restTemplate.getForEntity("http://provider/hello/" + UUID.randomUUID().toString(), String.class);
    }

    @GetMapping("/feign")
    public ResponseEntity<String> getFeignName () {
        return ResponseEntity.ok(haloFeignService.getName(UUID.randomUUID().toString()));
    }

}
