package org.andot.share.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * @author andot
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/{id}")
    public ResponseEntity<String> getName(@PathVariable("id") String id) {
        jdbcTemplate.update("INSERT INTO `order`(`name`) VALUES (?);", id);
        return ResponseEntity.ok("lucas --- " + id);
    }
}
