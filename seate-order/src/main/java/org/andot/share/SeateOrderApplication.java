package org.andot.share;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class SeateOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeateOrderApplication.class, args);
    }

}
