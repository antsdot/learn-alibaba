package org.andot.share.service;

import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author andot
 */
@Service("helloService")
public class HelloService {

    @Autowired
    private OrderService orderService;
    @Autowired
    private PayService payService;


    @GlobalTransactional
    public void add () {
        String uuid = UUID.randomUUID().toString();
        orderService.add(uuid);
        payService.add(uuid);
    }

}
