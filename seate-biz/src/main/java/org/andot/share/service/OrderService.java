package org.andot.share.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author andot
 */
@Service("orderService")
@FeignClient(name = "seate-order",  path = "/hello")
public interface OrderService {
    @GetMapping("/{id}")
    ResponseEntity<String> add (@PathVariable("id") String id);
}
