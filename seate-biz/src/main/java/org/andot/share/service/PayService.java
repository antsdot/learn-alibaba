package org.andot.share.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author andot
 */
@Service("payService")
@FeignClient(name = "seate-pay",  path = "/hello")
public interface PayService {
    @GetMapping("/{id}")
    ResponseEntity<String> add (@PathVariable("id") String id);
}
