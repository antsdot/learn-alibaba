package org.andot.share.controller;

import org.andot.share.service.HelloService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author andot
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    @Resource
    private HelloService helloService;

    @GetMapping("")
    public ResponseEntity<String> getName() {
        helloService.add();
        return ResponseEntity.ok("lucas --- success");
    }
}
