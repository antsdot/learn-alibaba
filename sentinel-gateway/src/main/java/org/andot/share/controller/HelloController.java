package org.andot.share.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author andot
 */
@RestController
@RequestMapping("/get")
public class HelloController {

    @GetMapping("/{id}")
    public ResponseEntity<String> getName(@PathVariable("id") String id) {
        return ResponseEntity.ok("lucas - sentinel - feign -- " + id);
    }
}
