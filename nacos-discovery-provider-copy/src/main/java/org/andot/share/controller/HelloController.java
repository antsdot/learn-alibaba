package org.andot.share.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author andot
 */
@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping("/{id}")
    public ResponseEntity<String> getName(@PathVariable("id") String id) {
        return ResponseEntity.ok("lucas copy --- " + id);
    }
}
