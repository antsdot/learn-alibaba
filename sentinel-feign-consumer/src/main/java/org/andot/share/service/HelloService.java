package org.andot.share.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author andot
 */
@Service("helloService")
@FeignClient(name = "sentinal-feign-provider", path = "/hello")
public interface HelloService {
    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    String getName (@PathVariable("id") String id);
}
