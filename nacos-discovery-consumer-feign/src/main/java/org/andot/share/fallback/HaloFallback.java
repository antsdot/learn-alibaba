package org.andot.share.fallback;

import org.andot.share.service.HaloFeignService;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author andot
 */
@Component
@RequestMapping("fallback/")
public class HaloFallback implements HaloFeignService {

    @Override
    public String getName(String id) {
        return "默认值：default";
    }
}
